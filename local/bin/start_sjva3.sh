#!/bin/sh

if [ -z "${SJVA3_HOME}" ]; then
	. /usr/local/bin/docker-env
fi

while [ \( "${NO_CELERY}" = "N" -a -z "$(pgrep -f "redis-server")" \) \
	-o \( "${NO_CELERY}" = "N" -a -z "$(pgrep -f "python -m celery --app=sjva3.celery")" \) ]
do
	sleep 1
done

cd ${SJVA3_HOME}

COUNT=0
while [ 1 ];
do
	#Update Logic을
	#echo "Updating SVJA2"
	#wget --no-check-certificate https://github.com/soju6jan/SJVA3/archive/master.tar.gz -q -O - | tar zx -C ${SJVA3_HOME}
	#rm -rf ${SJVA3_HOME}/*.sh ${SJVA3_HOME}/*.cmd
	#update
	find . -name "index.lock" -exec rm -f {} \;
	git reset --hard HEAD
	git pull
	chmod 777 .
	if [ -d "./data/custom" ]; then
		chmod -R +x ./data/custom
	fi
	if [ -e update_requirements.txt ]; then
		pip install -r update_requirements.txt
	fi
	if [ -e migrations ]; then
		python -m flask db migrate || true &> /dev/null
		python -m flask db upgrade || true
	fi
	if [ -z "$(grep "'인증되었습니다. (회원등급:10, 포인트:999)'" lib/system/logic_auth.py)" ]; then
		sed -i -e "s/    def get_auth_status(retry=True):/    def get_auth_status(retry=True):\n        return {'ret': True, 'desc': '인증되었습니다. (회원등급:10, 포인트:999)', 'level': 10, 'point': 999}/g" lib/system/logic_auth.py
	fi
	python sjva3.py --use_gevent $([ "${NO_GEVENT}" = "Y" ] && echo "false" || echo "true") --use_celery $([ "${NO_CELERY}" = "Y" ] && echo "false" || echo "true") --repeat ${COUNT} 
	RESULT=$?
	echo "PYTHON EXIT CODE : ${RESULT}.............."
	if [ "$RESULT" = "0" ]; then
		echo 'FINISH....'
		break
	else
		echo 'REPEAT....'
	fi
	COUNT=`expr $COUNT + 1`
done


#!/bin/sh

if [ -z "${SJVA3_HOME}" ]; then
	. /usr/local/bin/docker-env
fi

if [ "${NO_CELERY}" = "N" ]; then
	mkdir -p /app/SJVA3/data/redis
	cd /app/SJVA3/data/redis
	exec redis-server --port ${REDIS_PORT}
fi


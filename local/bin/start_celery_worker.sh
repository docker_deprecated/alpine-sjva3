#!/bin/sh

if [ -z "${SJVA3_HOME}" ]; then
	. /usr/local/bin/docker-env
fi

if [ "${NO_CELERY}" = "N" ]; then
	while [ -z "$(pgrep -f "redis-server")" ]
	do
		sleep 1
	done

	cd ${SJVA3_HOME}

	exec python -m celery --app=${CELERY_APP:-sjva3.celery} --workdir=${SJVA3_HOME} worker --logfile=${SJVA3_HOME}/data/log/celery%I.log --loglevel=INFO -c ${CELERY_WORKER_COUNT:-2}
fi


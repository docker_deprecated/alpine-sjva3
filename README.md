# alpine-sjva3
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-sjva3)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-sjva3)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-sjva3/x64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [SJVA3](https://sjva.me/)
    - SVJA2 is  ...



----------------------------------------
#### Run

```sh
docker run -d \
           -p 9999:9999/tcp \
           -v /conf.d:/conf.d \
           forumi0721/alpine-sjva3:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:9999/](http://localhost:9999/)
    - Default username/password : svja2/sjva3



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 9999/tcp           | SJVA3 port                                       |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| NO_GEVENT          | Do not use gevent                                |
| NO_CELERY          | Do not use celery                                |

